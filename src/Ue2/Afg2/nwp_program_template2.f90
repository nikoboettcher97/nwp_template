!------------------------------------------------------------------------------!
! Numerical Weather Prediction - Programming Excercise
!------------------------------------------------------------------------------!
! Tolles Programm für das Praktik

! TODO: Add description
!  Add a proper description of program. Should include how to steer (including
!  required input) and what to expect as output.
!  2018-02-14, TG

!------------------------------------------------------------------------------!
! Author: Tobias Gronemeier
! Date  : 2018-02-14
!------------------------------------------------------------------------------!
! NOTE: Code compilation
!  When compiling the code, NetCDF libraries must be included/linked like so
!    ifort nwp_program.f90 -I <path-to-netcdf>/include -L <path-to-netcdf>/lib -lnetcdff
!  2018-02-07, TG

PROGRAM nwp_program

   USE netcdf

   IMPLICIT NONE


   !- Declare variables
   CHARACTER(LEN=100) :: file_out = "nwp_output"   !< name of output file (namelist param)

   INTEGER :: i               !< loop index
   INTEGER :: j               !< loop index
   INTEGER :: k               !< loop index
   INTEGER :: t               !< loop index
   INTEGER :: k_max = 5000  !< maximum tteration count of SOR method (namelist param)
   INTEGER :: nx = 399         !< number of grid points along x (namelist param)
   INTEGER :: ny = 200         !< number of grid points along y (namelist param)
   INTEGER :: nt		!< number of time steps
   INTEGER :: fall          !< für die CASE-Abfrage
   INTEGER :: ioerr           !<Fehlerfassung

   REAL :: beta               !< beta parameter
   REAL :: delta = 30000.0    !< grid width (namelist param)
   REAL :: dt                 !< time-step width

   
   !< timestep of data output
   REAL :: f_0                !< Coriolis parameter at y=0
   REAL :: latitude = 52.0    !< latitude (namelist param)
   REAL :: omega              !< relaxation factor of SOR method
   REAL :: t_end              !< time to simulate (s)
   REAL :: t_sim              !< simulated time (s)
   REAL :: time_end = 24.0    !< time to simulate (h, namelist param)
   REAL :: time_dt_do = 24.0  !< time interval of output (h, namelist param)
   REAL :: u_bg               !< background wind speed
   REAL :: u_max              !< maximum wind speed
   INTEGER :: max_it
   REAL :: phi_A
   REAL :: lx
   REAL :: ly
   REAL :: c_t                  !< Verlagerungsgeschwindigkeit theoretisch
   REAL :: c_m                  !< Verlagerungsgeschwindigkeit modell
   REAL, DIMENSION(2) :: m1     !< Position des Maximums von phi bei t = 0
   REAL, DIMENSION(2) :: m2     !<Position des Maximums von phi bei t = 24

   
   REAL, DIMENSION(:), ALLOCATABLE :: f         !< Coriolis parameter along y

   REAL, DIMENSION(:,:), ALLOCATABLE :: phi     !< geopotential
   REAL, DIMENSION(:,:), ALLOCATABLE :: phi_alt !< geopotential von k-1
   REAL, DIMENSION(:,:), ALLOCATABLE :: u       !< wind speed along x
   REAL, DIMENSION(:,:), ALLOCATABLE :: v       !< wind speed along y
   REAL, DIMENSION(:,:), ALLOCATABLE :: zeta    !< vorticity at time t
   REAL, DIMENSION(:,:), ALLOCATABLE :: zeta_m  !< vorticity at time t-1
   REAL, DIMENSION(:,:), ALLOCATABLE :: zeta_p  !< vorticity at time t+1
!    REAL, DIMENSION(:,:), ALLOCATABLE :: c_t     !< Verlagerungsgeschwindigkeit theoretisch
!    REAL, DIMENSION(:,:), ALLOCATABLE :: c_m     !< Verlagerungsgeschwindigkeit modell

   REAL, DIMENSION(:,:), ALLOCATABLE :: phi_ref  !< reference geopotential

   REAL, PARAMETER :: g = 9.81      !< acceleration of gravity
   REAL, PARAMETER :: pi = 3.14159  !< pi


   !- Start
   WRITE(*, '(A//A)') ' --- NWP program, V0.0.1 ---',  &
                      ' --- start forecast...'

   !- Define and read init-namelist
   namelist / INIT / delta, file_out, k_max, latitude, nx, ny, time_dt_do,     &
                     time_end, fall

   OPEN(1, FILE='./nwp_prog.init', STATUS='OLD', FORM='FORMATTED', IOSTAT=ioerr)
!    IF (ioerr /= 0) THEN
!       CALL error_message('err11')
!    ENDIF

   READ(1, NML=INIT, IOSTAT=ioerr)
!    IF (ioerr /= 0) THEN
!       CALL error_message('err12')
!    ENDIF

   CLOSE(1)


   !- Initialize variables
   ! Convert from hours into seconds
   t_end = time_end * 3600.0 ![t_end]=s
   ! ...

   ! wind speed setup
   u_max = 10.0      ![umax]=m/s
   ! ...

   
   ALLOCATE(phi(0:ny,-1:nx+1)) 
   ALLOCATE(phi_alt(0:ny,-1:nx+1))
   ALLOCATE(f(0:ny))
   ALLOCATE(zeta(0:ny,-1:nx+1))
   ALLOCATE(zeta_m(0:ny,-1:nx+1))
   ALLOCATE(zeta_p(0:ny,-1:nx+1))  
   ALLOCATE(phi_ref(0:ny, -1:nx+1))
   ALLOCATE(u(0:ny , -1:nx+1))
   ALLOCATE(v(0:ny, -1:nx+1))
!    ALLOCATE(c_t(0:ny , -1:nx+1))
!    ALLOCATE(c_m(0:ny, -1:nx+1))
   
   
   f(:) =  0
   f_0 = 1.0E-4
   phi(:,:) = 0.0
   
   SELECT CASE (fall)
    
!       CASE(1)
! 	!lineares Geschwindigkeitsfeld
! 	zeta(:,:) = 0.0
! 	phi(0,:) = 0.0
! 	phi(ny,:) = 50.0
! 	
! 	!Brechenung von phi für eine 1D-Stömung(zeta=0) --> phi_ref = phi(ly)/ly * y mit Randbedingungne und
! 	!ly = delta * ny
! 	!y = delta * j   folgt:
! 	! --> phi_ref = 50/ny * j
! 	
! 	
! 	DO j= 0, ny
! 	  phi_ref(j,:) = (50.0/ny)*j
! 	END DO
! 	  
! 	  
!       CASE(2)
! 	!Scherströmung
! 	zeta(:,:) = 1.0E-4
! 	phi(0,:) = 0.0
! 	phi(ny,:) = 0.0
! 	
! 	
! 	
! 	!Brechenung von phi für eine 1D-Stömung(zeta=const.) --> phi = 1/2*a*y^2 + by + c mit Randbedingungne folgt:
! 	! a = 10^-8
! 	! b = 1/2 a*ly
! 	! c = 0
! 	
! 	DO i = 0, nx
! 	  DO j = 0, ny
! 	  phi_ref(j,i) = 0.5 * 1.0E-8 * (delta * j)**2 - 0.5 * 1.0E-8 * ny * delta**2 * j
! 	  END DO
! 	END DO
	
	
      CASE(3)
	!2D-Problem
	phi(0,:) = 0.0
	phi(ny,:) = 0.0
	
	
	!Berechung von phi um zeta zu bestimmnen mit phi = phi_A * sin(2pi/lx * x) * sin(2pi/ly *y)
	!mit phi_A = u_max * ly * f /pi
	!Bestmming von zeta mit: zeta = -1/f * (4pi**2 / lx**2 + pi**2/ly) * phi_ref
	
	ly = 2.0 *  ny * delta
	lx =  nx * delta
	u_max = 10.0
    beta = 1.62E-11
    u_bg = 0.0
    
    
    c_t = u_bg - beta * 1.0 / ((2.0 * pi / lx)**2 + (2.0 * pi / ly)**2)
    
    Write(*,*) c_t
    


	
	DO j = 0,ny
	f(j) = f_0 + beta * j * delta
	END DO
	
	
	DO i=0,nx
	  DO j=0,ny
	    phi_A = u_max * ly * f_0 / (2.0 * pi)
	    phi_ref(j,i) = phi_A * SIN((2.0 * pi) / lx * i * delta) * SIN((2.0 * pi) / ly * j * delta) 
	    zeta(j,i) = - 1.0/1.0E-4 * (4.0*pi**2 / lx**2 + 4.0*pi**2 / ly**2) * phi_ref(j,i) 
	  END DO
	END DO  
	
        
    zeta_p(:,:) = 0.0
    
    zeta(:,-1) = zeta(:,nx)
    zeta(:,nx+1) = zeta(:,0)
      
    END SELECT
! 	
	

   !- Read input data
   ! CALL read_input_data(...)


   !- Open output file
   CALL data_output('open', file_out)

   CALL get_geopot(k_max)
   
   CALL get_wind()
   
   CALL data_output('write', file_out)

   !- Start forecast
   t_sim = 0.0
   nt = 0

   
   dt = timestep(u, v)
   
   nt = t_end/dt
   
   
    m1 = MAXLOC(phi)
    

    

   
   Write (*,*) dt
   
    DO t = 1, nt

       CALL get_vorticity
 
       CALL get_geopot(k_max)

       CALL get_wind()
       
       CALL data_output('write', file_out)
       
       
       
       write(*,*) t
       
    ENDDO
    
    m2 = MAXLOC(phi)
      
    c_m = (m2(2)-m1(2))*30000.0/(24.0*3600.0)
    
    WRITE (*,*) c_m

   !- Finalize output
   CALL data_output('close', file_out)


   !- End
   WRITE(*, '(A)') ' --- Finished! :-) ---'


CONTAINS
!- Functions and subroutines

   REAL FUNCTION timestep(u, v)
   !Calculate timestep
   REAL, DIMENSION(0:ny,-1:nx+1), INTENT(IN) :: u
   REAL, DIMENSION(0:ny,-1:nx+1), INTENT(IN) :: v 

   
      timestep = 0.5 * MIN((delta / MAXVAL(u)), (delta / MAXVAL(v)))
      
   END FUNCTION timestep

   !----------------------------------------------------------------------------

   ! SUBROUTINE error_message(...)
   ! ! Print error message and close program
      ! ....
   ! END SUBROUTINE error_message

   !----------------------------------------------------------------------------

   SUBROUTINE get_geopot(max_it)
   ! Calculate geopotential via SOR method

	  INTEGER, INTENT (IN) :: max_it  !< maximum iteration count
	  REAL :: phi_diff
	  REAL :: diff

	  omega = 2.0 - ((2.0*pi)/sqrt(2.0)) * sqrt(1.0/(nx + 1.0)**2 + 1.0/(ny + 1.0))
	  diff = 0.0
	  

	  phi_alt(:,:) = phi(:,:)
	  phi_diff = 0.0

	  
	  OPEN(10, file="test.txt", action="write")
! 	  
! 	 Write (*,*) "Hallo"
	  
	!SOR-Verfahren (konvergiert noch zu langsam!) 
	DO k = 1, k_max
	 
		DO i = 0,nx
			DO j = 1,ny-1
				phi(j,i) = phi(j,i)*(1.0-omega) + (omega/4.0)*(phi(j+1,i) + phi(j-1,i) + phi(j,i+1) + phi(j,i-1)&
				- f(j) * delta**2 * zeta(j,i) )
		
			END DO
		END DO

		!zyklische Randbedingungen
		phi(:,-1) = phi(:,nx)
		phi(:,nx+1) = phi(:,0)
		
		!Abbruch Kriterium
		phi_diff = MAXVAL(ABS(phi_alt - phi))
		IF (phi_diff .lt. diff) Then
			exit
			
		END IF
		phi_alt = phi
	 
	END DO 
	 

! 		WRITE (*,*) k
!  		Write (10, '(f10.5)') phi(1,:)	  

	END SUBROUTINE get_geopot
   !----------------------------------------------------------------------------

   SUBROUTINE get_vorticity
   ! Calculate vorticity via prognostic equation
        
        
        
      IF (t == 1) THEN
      
      DO i = 0,nx
            DO j = 1,ny-1
                   
                        zeta_p(j,i) = zeta(j,i) - dt * &
                        (u(j,i)* (zeta(j,i+1) - zeta(j,i-1)) / (2.0 * delta)&
                        + v(j,i) * (zeta(j+1,i) - zeta(j-1,i)) / (2.0 * delta) + v(j,i) * beta)
               
 
             END DO
      END DO
    
    
      
      ELSE
   
      DO i = 0,nx
            DO j = 1,ny-1
            
                
                zeta_p(j,i) = zeta_m(j,i) - 2.0 * dt *&
                ( u(j,i)* (zeta(j,i+1) - zeta(j,i-1)) / (2.0 * delta) &
                + v(j,i) *(zeta(j+1,i) - zeta(j-1,i)) / (2.0 * delta) + v(j,i) * beta)
                
            END DO
      END DO
      
      END IF
   
      
      zeta_p(:,-1) = zeta_p(:,nx)
      zeta_p(:,nx+1) = zeta_p(:,0)
      
      zeta_m = zeta
      zeta = zeta_p
      
      
   END SUBROUTINE get_vorticity

   !----------------------------------------------------------------------------

    SUBROUTINE get_wind()
!     REAL, INTENT(IN) ::a

   ! ! Calculate wind speed components via geostrophic wind relation
      
     DO i = 0, nx
        DO j = 1, ny-1
            u(j,i) = -1.0/f(j) * (phi(j+1,i) - phi(j-1,i)) / (2.0 * delta)
            v(j,i) = 1.0/ f(j) * (phi(j,i+1) - phi(j,i-1)) / (2.0 * delta)
	  
      END DO
     END DO
     
     
     
     
     
      u(:,-1) = u(:,nx)
      u(:,nx+1) = u(:,0)
     
      v(:,-1) = v(:,nx)
      v(:,nx+1) = v(:,0)
      
   END SUBROUTINE get_wind

   !----------------------------------------------------------------------------

   ! SUBROUTINE read_input_data(...)
   ! ! Read input from mesoscale model
      ! ...
   ! SUBROUTINE read_input_data

   !----------------------------------------------------------------------------

   SUBROUTINE data_output(action, output)
   ! Data output to NetCDF file

      CHARACTER(LEN=*), INTENT(IN) :: action !< flag to steer routine (open/close/write)
      CHARACTER(LEN=*), INTENT(IN) :: output !< output file name prefix

      INTEGER, SAVE :: do_count=0      !< counting output
      INTEGER, SAVE :: id_dim_time     !< ID of dimension time
      INTEGER, SAVE :: id_dim_x        !< ID of dimension x
      INTEGER, SAVE :: id_dim_y        !< ID of dimension time
      INTEGER, SAVE :: id_file         !< ID of NetCDF file
      INTEGER, SAVE :: id_var_phi      !< ID of geopotential
      INTEGER, SAVE :: id_var_phiref   !< ID of reference geopotential
      INTEGER, SAVE :: id_var_time     !< ID of time
      INTEGER, SAVE :: id_var_u        !< ID of wind speed along x
      INTEGER, SAVE :: id_var_v        !< ID of wind speed along y
      INTEGER, SAVE :: id_var_x        !< ID of x
      INTEGER, SAVE :: id_var_y        !< ID of y
      INTEGER, SAVE :: id_var_zeta     !< ID of vorticity
      INTEGER, SAVE :: nc_stat         !< status flag of NetCDF routines

      INTEGER :: tt                    !< time index of ref. geopot.

      REAL, DIMENSION(:), ALLOCATABLE :: netcdf_data_1d  !< 1D output array

      REAL, DIMENSION(:,:), ALLOCATABLE :: netcdf_data_2d  !< 2D output array


      SELECT CASE (TRIM(action))

         !- Initialize output file
         CASE ('open')

            !- Delete any pre-existing output file
            OPEN(20, FILE=TRIM(output)//'.nc')
            CLOSE(20, STATUS='DELETE')

            !- Open file
            nc_stat = NF90_CREATE(TRIM(output)//'.nc', NF90_NOCLOBBER, id_file)
            IF (nc_stat /= NF90_NOERR)  PRINT*, '+++ netcdf error'

            !- Write global attributes
            nc_stat = NF90_PUT_ATT(id_file, NF90_GLOBAL,  &
                                    'Conventions', 'COARDS')
            nc_stat = NF90_PUT_ATT(id_file, NF90_GLOBAL,  &
                                    'title', 'barotropic nwp-model')

            !- Define time coordinate
            nc_stat = NF90_DEF_DIM(id_file, 'time', NF90_UNLIMITED,  &
                                    id_dim_time)
            nc_stat = NF90_DEF_VAR(id_file, 'time', NF90_DOUBLE,  &
                                    id_dim_time, id_var_time)
            nc_stat = NF90_PUT_ATT(id_file, id_var_time, 'units',  &
                                    'seconds since 1900-1-1 00:00:00')

            !- Define spatial coordinates
            nc_stat = NF90_DEF_DIM(id_file, 'x', nx+1, id_dim_x)
            nc_stat = NF90_DEF_VAR(id_file, 'x', NF90_DOUBLE,  &
                                    id_dim_x, id_var_x)
            nc_stat = NF90_PUT_ATT(id_file, id_var_x, 'units', 'meters')

            nc_stat = NF90_DEF_DIM(id_file, 'y', ny+1, id_dim_y)
            nc_stat = NF90_DEF_VAR(id_file, 'y', NF90_DOUBLE,  &
                                    id_dim_y, id_var_y)
            nc_stat = NF90_PUT_ATT(id_file, id_var_y, 'units', 'meters')

            !- Define output arrays
            nc_stat = NF90_DEF_VAR(id_file, 'phi', NF90_DOUBLE,           &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_phi)
            nc_stat = NF90_PUT_ATT(id_file, id_var_phi,  &
                                    'long_name', 'geopotential at 500hPa')
            nc_stat = NF90_PUT_ATT(id_file, id_var_phi,  &
                                    'short_name', 'geopotential')
            nc_stat = NF90_PUT_ATT(id_file, id_var_phi, 'units', 'm2/s2')

            nc_stat = NF90_DEF_VAR(id_file, 'phi_ref', NF90_DOUBLE,       &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_phiref)
            nc_stat = NF90_PUT_ATT(id_file, id_var_phiref,  &
                                    'long_name',            &
                                    'reference geopotential at 500hPa')
            nc_stat = NF90_PUT_ATT(id_file, id_var_phiref,  &
                                    'short_name', 'ref. geopotential')
            nc_stat = NF90_PUT_ATT(id_file, id_var_phiref, 'units', 'm2/s2')

            nc_stat = NF90_DEF_VAR(id_file, 'zeta', NF90_DOUBLE,          &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_zeta)
            nc_stat = NF90_PUT_ATT(id_file, id_var_zeta,  &
                                    'long_name', 'vorticity at 500hPa')
            nc_stat = NF90_PUT_ATT(id_file, id_var_zeta,  &
                                    'short_name', 'vorticity')
            nc_stat = NF90_PUT_ATT(id_file, id_var_zeta, 'units', '1/s')

            nc_stat = NF90_DEF_VAR(id_file, 'u', NF90_DOUBLE,             &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_u)
            nc_stat = NF90_PUT_ATT(id_file, id_var_u,  &
                                    'long_name',       &
                                    'u component of wind')
            nc_stat = NF90_PUT_ATT(id_file, id_var_u, 'short_name', 'u')
            nc_stat = NF90_PUT_ATT(id_file, id_var_u, 'units', 'm/s')

            nc_stat = NF90_DEF_VAR(id_file, 'v', NF90_DOUBLE,             &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_v)
            nc_stat = NF90_PUT_ATT(id_file, id_var_v,  &
                                    'long_name',       &
                                    'v component of wind')
            nc_stat = NF90_PUT_ATT(id_file, id_var_v, 'short_name', 'v')
            nc_stat = NF90_PUT_ATT(id_file, id_var_v, 'units', 'm/s')


            !- Leave define mode
            nc_stat = NF90_ENDDEF(id_file)

            !- Write axis to file
            ALLOCATE(netcdf_data_1d(0:nx))

            !- x axis
            DO  i = 0, nx
               netcdf_data_1d(i) = i * delta
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_x, netcdf_data_1d,  &
                                    start = (/1/), count = (/nx+1/))
            !- y axis
            DO  j = 0, ny
               netcdf_data_1d(j) = j * delta
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_y, netcdf_data_1d,  &
                                    start = (/1/), count = (/ny+1/))

            DEALLOCATE(netcdf_data_1d)


         !- Close NetCDF file
         CASE ('close')

            nc_stat = NF90_CLOSE(id_file)


         !- Write data arrays to file
         CASE ('write')

            ALLOCATE(netcdf_data_2d(0:nx,0:ny))

            do_count = do_count + 1

            ! !- Write time
            nc_stat = NF90_PUT_VAR(id_file, id_var_time, (/t_sim/),  &
                                    start = (/do_count/),               &
                                    count = (/1/))

            !- Write geopotential
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = phi(j,i)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_phi, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),          &
                                    count = (/nx+1, ny+1, 1/))

            ! !- Write reference geopotential
            ! ! tt =...     ! time index of reference geopotential
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = phi_ref(j,i)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_phiref, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),             &
                                    count = (/nx+1, ny+1, 1/))

            !- Write vorticity
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = zeta(j,i)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_zeta, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),           &
                                    count = (/nx+1, ny+1, 1/))

            ! !- Write u
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = u(j,i)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_u, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),        &
                                    count = (/nx+1, ny+1, 1/))

            ! !- Write v
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = v(j,i)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_v, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),        &
                                    count = (/nx+1, ny+1, 1/))

            DEALLOCATE(netcdf_data_2d)


         !- Print error message if unknown action selected
         CASE DEFAULT

            WRITE(*, '(A)') ' ** data_output: action "'//  &
                              TRIM(action)//'"unknown!'

      END SELECT

   END SUBROUTINE data_output


END PROGRAM nwp_program
