!------------------------------------------------------------------------------!
! Numerical Weather Prediction - Programming Excercise
!------------------------------------------------------------------------------!
! Tolles Programm für das Praktik

! TODO: Add description
!  Add a proper description of program. Should include how to steer (including
!  required input) and what to expect as output.
!  2018-02-14, TG

!------------------------------------------------------------------------------!
! Author: Tobias Gronemeier
! Date  : 2018-02-14
!------------------------------------------------------------------------------!
! NOTE: Code compilation
!  When compiling the code, NetCDF libraries must be included/linked like so
!    ifort nwp_program.f90 -I <path-to-netcdf>/include -L <path-to-netcdf>/lib 
!	 -lnetcdff
!  2018-02-07, TG

PROGRAM nwp_program

   USE netcdf

   IMPLICIT NONE


   !- Declare variables
   CHARACTER(LEN=100) :: file_out = "nwp_output"   !< name of output file (NP)


   INTEGER :: i               !< loop index
   INTEGER :: j               !< loop index
   INTEGER :: k               !< loop index
   INTEGER :: t               !< loop index
   INTEGER :: k_max           !< maximum tteration count of SOR method (NP)
   INTEGER :: nx              !< number of grid points along x (NP)
   INTEGER :: ny              !< number of grid points along y (NP)
   INTEGER :: nt		      !< number of time steps
   INTEGER :: fall            !< für die CASE-Abfrage
   INTEGER :: ioerr           !< Fehlerfassung
   INTEGER :: count           !< Variabel für die Zeitmessung
   INTEGER :: count_rate      !< number of clock ticks per second
   INTEGER :: nt_ref          !< Anzahl der Zeitschritte des Globalmodells (NP)
 
   REAL :: beta               !< beta parameter
   REAL :: delta              !< grid width (NP)
   REAL :: dt                 !< time-step width
   REAL :: dt_do			  !< timestep of data output
   
   REAL :: diff_SOR           !< Zeitmessung für das SOR-Verfahren
   REAL :: SOR_t = 0.0        !< Zeitmessung für das SOR-Verfahren (SUM)
   REAL :: diff_VOR           !< Zeitmessung für das Zeitschrittverfahren
   REAL :: VOR_t = 0.0        !< Zeitmessung für das Zeitschrittverfahren (SUM) 
   REAL :: diff_WIND          !< Zeitmessung für die Berechnung von u,v
   REAL :: WIND_t = 0.0       !< Zeitmessung für die Berechnung von u,v (SUM)
   REAL :: diff_DAT			  !< Zeitmessung für die Datenausgabe
   REAL :: DAT_t = 0.0		  !< Zeitmessung für die Datenausgabe (SUM)
   REAL :: diff_GES			  !< Zeitmessung der Gesamtlaufzeit
   REAL :: GES_t = 0.0		  !< Zeitmessung der Gesamtlaufzeit (SUM)
   
   REAL :: f_0                !< Coriolis parameter at y=0
   REAL :: latitude           !< latitude (NP)
   REAL :: omega              !< relaxation factor of SOR method
   REAL :: t_end              !< time to simulate (s)
   REAL :: t_sim              !< simulated time (s)
   REAL :: time_end           !< time to simulate (h, NP)
   REAL :: time_dt_do         !< time interval of output (h, NP)
   REAL :: u_bg               !< background wind speed
   REAL :: u_max              !< maximum wind speed
   REAL :: phi_A              !< Amplitude
   REAL :: lx                 !< Wellenlänge entlang x
   REAL :: ly                 !< Wellenlänge entlang y
   REAL :: c_t                !< Verlagerungsgeschwindigkeit theoretisch
   REAL :: c_m                !< Verlagerungsgeschwindigkeit modell
   REAL, DIMENSION(2) :: m1   !< Position des Maximums von phi bei t = 0
   REAL, DIMENSION(2) :: m2   !< Position des Maximums von phi bei t = 24
 
   REAL, DIMENSION(:), ALLOCATABLE :: f         !< Coriolis parameter along y

   REAL, DIMENSION(:,:), ALLOCATABLE :: phi     !< geopotential
   REAL, DIMENSION(:,:), ALLOCATABLE :: phi_alt !< geopotential von k-1
   REAL, DIMENSION(:,:), ALLOCATABLE :: u       !< wind speed along x
   REAL, DIMENSION(:,:), ALLOCATABLE :: v       !< wind speed along y
   REAL, DIMENSION(:,:), ALLOCATABLE :: zeta    !< vorticity at time t
   REAL, DIMENSION(:,:), ALLOCATABLE :: zeta_m  !< vorticity at time t-1
   REAL, DIMENSION(:,:), ALLOCATABLE :: zeta_p  !< vorticity at time t+1

   REAL, DIMENSION(:,:,:), ALLOCATABLE :: phi_ref  !< reference geopotential

   REAL, PARAMETER :: g = 9.81      !< acceleration of gravity
   REAL, PARAMETER :: pi = 3.14159  !< pi


   !- Start
   
   !Beginn Zeitmessung der Gesamtlaufzeit
   CALL SYSTEM_CLOCK(count,count_rate)
      diff_GES = REAL(count)/REAL(count_rate)
   
   WRITE(*, '(A//A)') ' --- NWP program, V0.0.1 ---',  &
                      ' --- start forecast...'

   !- Define and read init-namelist
   namelist / INIT / delta, file_out, k_max, latitude, nx, ny, time_dt_do,     &
                     time_end, nt_ref

   OPEN(1, FILE='./nwp_prog.init', STATUS='OLD', FORM='FORMATTED', IOSTAT=ioerr)
   IF (ioerr /= 0) THEN
      CALL error_message('err11')
   ENDIF

   READ(1, NML=INIT, IOSTAT=ioerr)
   IF (ioerr /= 0) THEN
      CALL error_message('err12')
   ENDIF

   CLOSE(1)
   
   
   
   ALLOCATE(phi(-1:ny+1,-1:nx+1)) 
   ALLOCATE(phi_alt(-1:ny+1,-1:nx+1))
   ALLOCATE(f(0:ny))
   ALLOCATE(zeta(-1:ny+1,-1:nx+1))
   ALLOCATE(zeta_m(-1:ny+1,-1:nx+1))
   ALLOCATE(zeta_p(-1:ny+1,-1:nx+1))  
   ALLOCATE(phi_ref(-1:ny+1, -1:nx+1,0:nt_ref))
   ALLOCATE(u(-1:ny+1 , -1:nx+1))
   ALLOCATE(v(-1:ny+1, -1:nx+1))
 
   
   
   !- Initialize variables
   ! Convert from hours into seconds
   t_end = time_end * 3600.0	![t_end]=s
   dt_do = time_dt_do * 3600.0	![dt_do]=s

   ! wind speed setup
   u_max = 10.0      ![umax]=m/s


   f(:) =  0.0
   f_0 = 7.27E-5
   beta = 1.62E-11
   
   !Randbedingungen
   phi(:,:) = 0.0
   phi_ref(:,:,:) = 0.0
   zeta(:,:) = 0.0
   zeta_p(:,:) = 0.0
   zeta_m(:,:) = 0.0
    


       

   !- Read input data
   CALL read_input_data('/home/gronemeier/Public/NWP/era_int_eu_20171024_20171026.nc')

   !- Open output file
   CALL data_output('open', file_out)
   
   !Initialisieren ders phi Feldes für t = 0
   phi(:,:) = phi_ref(:,:,0)
   
   !Berechnung des Coriolisparameter
   DO j = 0,ny
      f(j) = f_0 + beta * j * delta
   END DO
   
   
   DO i = 0,nx
      DO j = 0,ny
         zeta(j,i) = (1/f(j)) * ((phi(j,i+1) - 2.0 * phi(j,i)   &
         + phi(j,i-1))/delta**2 + (phi(j+1,i)   &
         - 2.0 * phi(j,i) + phi(j-1,i))/delta**2)
      END DO
   END DO
       
  
   CALL get_wind
   
   
   CALL data_output('write', file_out) 
   

   !- Start forecast
   t_sim = 0.0
   nt = 0

    
    
   DO WHILE (t_sim <= t_end)
      
      dt = timestep(u, v)
    
      nt = nt + 1
       
      CALL get_randwerte
       
      CALL get_vorticity
        
      CALL get_geopot(k_max)
       
      CALL get_wind
        
      t_sim = t_sim + dt
        
      IF (t_sim >= dt_do) THEN    
         CALL data_output('write', file_out) 
      END IF
      
   END DO


   !- Finalize output
   CALL data_output('close', file_out)

   
   
   CALL SYSTEM_CLOCK(count,count_rate)
	  diff_GES = REAL(count)/REAL(count_rate) - diff_GES
    
    
   WRITE (*,*) "Gesamtzeit beträgt: " , diff_GES, "s"
    
   WRITE (*,*) "Zeit SOR-Verfahren: ", SOR_t,"s", SOR_t/diff_GES * 100, "%"
    
   WRITE (*,*) "Zeit Vorticity: ", VOR_t,"s", VOR_t/diff_GES * 100,"%"
    
   WRITE (*,*) "Zeit Windgeschw.: ", WIND_t,"s", WIND_t/diff_GES * 100,"%"
    
   WRITE (*,*) "Zeit Datenausgabe: ", DAT_t,"s", DAT_t/diff_GES * 100,"%"
   
   

   !- End
   WRITE(*, '(A)') ' --- Finished! :-) ---'


CONTAINS
!- Functions and subroutines

   REAL FUNCTION timestep(u, v)
   !Calculate timestep
   REAL, DIMENSION(0:ny,-1:nx+1), INTENT(IN) :: u
   REAL, DIMENSION(0:ny,-1:nx+1), INTENT(IN) :: v 
   
      timestep = 0.5 * MIN((delta / MAXVAL(u)), (delta / MAXVAL(v)))
      
   END FUNCTION timestep

   !----------------------------------------------------------------------------

   SUBROUTINE error_message(type)
   ! Print error message and close program
     
	  CHARACTER(LEN=*), INTENT(IN) :: type
	 
	  Select CASE (type)
        
         CASE ('err11')
		    WRITE(*,*) "Namelist konnte nicht fehlerfrei geöffnet werden." 
		
		 CASE ('err12')
            WRITE(*,*) "Beim Einlesen der Namelist ist ein Fehelr aufgetreten."
	 
	  END SELECT
     
     
   END SUBROUTINE error_message

   !----------------------------------------------------------------------------

   SUBROUTINE get_geopot(max_it)
   ! Calculate geopotential via SOR method

	  INTEGER, INTENT (IN) :: max_it  !< maximum iteration count
	  REAL :: phi_diff
	  REAL :: epsilon


	  omega = 2.0 - ((2.0*pi)/sqrt(2.0)) * sqrt(1.0/(nx + 1.0)**2 &
	          + 1.0/(ny + 1.0))
	  epsilon = 0.001
	  

	  phi_alt(:,:) = phi(:,:)
	  phi_diff = 0.0
	
	  
	  CALL SYSTEM_CLOCK(count,count_rate)
         diff_SOR = REAL(count)/REAL(count_rate)
            
            
    
	  !SOR-Verfahren 
	  DO k = 1, k_max
	     DO i = 0,nx
            DO j = 0,ny
			   phi(j,i) = phi(j,i)*(1.0-omega) + (omega/4.0)*(phi(j+1,i)  &
			   + phi(j-1,i) + phi(j,i+1) + phi(j,i-1)  &
			   - f(j) * delta**2 * zeta(j,i))		
			END DO
		END DO

		
		!Abbruch Kriterium
		phi_diff = MAXVAL(ABS(phi_alt - phi))
		IF (phi_diff .lt. epsilon) Then
			exit
			
		END IF
		
		phi_alt = phi
	 
	  END DO 
	 
	 
      CALL SYSTEM_CLOCK(count,count_rate)
	     diff_SOR = REAL(count)/REAL(count_rate) - diff_SOR
        
      SOR_t = SOR_t + diff_SOR
    

   END SUBROUTINE get_geopot
   !----------------------------------------------------------------------------

   SUBROUTINE get_vorticity
   ! Calculate vorticity via prognostic equation
        
      CALL SYSTEM_CLOCK(count,count_rate)
         diff_VOR = REAL(count)/REAL(count_rate)  
        
      IF (nt == 1) THEN
      
         DO i = 1,nx-1
            DO j = 1,ny-1
               zeta_p(j,i) = zeta(j,i) - dt * (u(j,i) * (zeta(j,i+1)  &
               - zeta(j,i-1)) / (2.0 * delta)+ v(j,i) * (zeta(j+1,i)  &
               - zeta(j-1,i)) / (2.0 * delta) + v(j,i) * beta)
            END DO
         END DO
    
      ELSE
   
         DO i = 1,nx-1
            DO j = 1,ny-1
               zeta_p(j,i) = zeta_m(j,i) - 2.0 * dt * (u(j,i) * (zeta(j,i+1)  &
               - zeta(j,i-1)) / (2.0 * delta) + v(j,i) *(zeta(j+1,i)  &
               - zeta(j-1,i)) / (2.0 * delta) + v(j,i) * beta)
            END DO
         END DO
         
      END IF

      zeta_m = zeta
      zeta = zeta_p
      
      CALL SYSTEM_CLOCK(count,count_rate)
	     diff_VOR = REAL(count)/REAL(count_rate) - diff_VOR
        
      VOR_t = VOR_t + diff_VOR
    
            
   END SUBROUTINE get_vorticity

   !----------------------------------------------------------------------------

   SUBROUTINE get_wind()
    ! Calculate wind speed components via geostrophic wind relation
      
      CALL SYSTEM_CLOCK(count,count_rate)
         diff_WIND = REAL(count)/REAL(count_rate) 
      
      
      DO i = 0, nx
         DO j = 0, ny
            u(j,i) = -1.0/f(j) * (phi(j+1,i) - phi(j-1,i)) / (2.0 * delta)
            v(j,i) = 1.0/ f(j) * (phi(j,i+1) - phi(j,i-1)) / (2.0 * delta)
         END DO
      END DO
     
      
      CALL SYSTEM_CLOCK(count,count_rate)
	     diff_WIND = REAL(count)/REAL(count_rate) - diff_WIND
        
      WIND_t = WIND_t + diff_WIND
    
      
   END SUBROUTINE get_wind

   !----------------------------------------------------------------------------

   SUBROUTINE read_input_data(input)
   ! Read input from mesoscale model
      
      CHARACTER(LEN=*), INTENT(IN) :: input  !< name of input file

      INTEGER, SAVE :: id_file         !< ID of NetCDF file
      INTEGER, SAVE :: id_var_in       !< ID of input array
      INTEGER, SAVE :: nc_stat         !< status flag of NetCDF routines

      REAL, DIMENSION(-1:nx+1,-1:ny+1,0:nt_ref) :: var_in   !< input array

      WRITE(*, '(A)') ' --- Read input file...'

      !- Open NetCDF file
      nc_stat = NF90_OPEN( input, NF90_NOWRITE, id_file )
      IF ( nc_stat /= 0 )  WRITE(*,*) NF90_STRERROR(nc_stat)

      !- Get ID of array to read
      nc_stat = NF90_INQ_VARID( id_file, "phi", id_var_in )
      IF ( nc_stat /= 0 )  WRITE(*,*) NF90_STRERROR(nc_stat)

      !- Read array
      nc_stat = NF90_GET_VAR( id_file, id_var_in, var_in,        &
                            start = (/ 1, 1, 1 /),               &
                            count = (/ nx+3, ny+3, nt_ref+1 /) )
                            
      IF ( nc_stat /= 0 )  WRITE(*,*) NF90_STRERROR(nc_stat)

                            

      !- Resort input array and save to proper variable
      DO  t = 0, nt_ref
         DO  i = -1, nx+1
            DO  j = -1, ny+1
               phi_ref(j,i,t) = var_in(i,j,t)
            ENDDO
         ENDDO
      ENDDO

      WRITE(*, '(A)') '     ...finished'

      
    END SUBROUTINE read_input_data

   !----------------------------------------------------------------------------
   SUBROUTINE get_randwerte
   
      REAL, DIMENSION(:,:), ALLOCATABLE :: zeta_r  !zur Bestimmung der Randwerte
      INTEGER :: t_int
   
      ALLOCATE(zeta_r(0:ny,0:nx))
   
   
      t_int =  INT(t_sim/(6.0 * 3600.0))
   
   
      !Oberer Rand
      DO i = -1, nx+1
         phi(ny,i) = phi_ref(ny,i,t_int) + (phi_ref(ny,i,t_int+1)  &
         - phi_ref(ny,i,t_int))/(21600.0) * (t_sim - (t_int * 6.0 * 3600.0))
     
         phi(ny+1,i) = phi_ref(ny+1,i,t_int) + (phi_ref(ny+1,i,t_int+1)  &
         - phi_ref(ny+1,i,t_int))/(21600.0) * (t_sim - (t_int * 6.0 * 3600.0))
     
         phi(ny-1,i) = phi_ref(ny-1,i,t_int) + (phi_ref(ny-1,i,t_int+1)&
         - phi_ref(ny-1,i,t_int))/(21600.0) * (t_sim - (t_int * 6.0 * 3600.0))
      END DO
   
   
      !Unterer Rand
      DO i = -1, nx+1
         phi(0,i) = phi_ref(0,i,t_int) + (phi_ref(0,i,t_int+1)  &
         - phi_ref(0,i,t_int))/(21600.0) * (t_sim - (t_int * 6.0 * 3600.0))
        
         phi(1,i) = phi_ref(1,i,t_int) + (phi_ref(1,i,t_int+1)  &
         - phi_ref(1,i,t_int))/(21600.0) * (t_sim - (t_int * 6.0 * 3600.0))
        
         phi(-1,i) = phi_ref(-1,i,t_int) + (phi_ref(-1,i,t_int+1)  &
         - phi_ref(-1,i,t_int))/(21600.0) * (t_sim - (t_int * 6.0 * 3600.0))
      END DO
    
    
    
      !Rechter Rand
      DO j = -1, ny+1
         phi(j,nx) = phi_ref(j,nx,t_int) + (phi_ref(j,nx,t_int+1)  &
         - phi_ref(j,nx,t_int))/(21600.0) * (t_sim - (t_int * 6.0 * 3600.0))
        
         phi(j,nx+1) = phi_ref(j,nx+1,t_int) + (phi_ref(j,nx+1,t_int+1)  &
         - phi_ref(j,nx+1,t_int))/(21600.0) * (t_sim - (t_int * 6.0 * 3600.0))
        
         phi(j,nx-1) = phi_ref(j,nx-1,t_int) + (phi_ref(j,nx-1,t_int+1)  &
         - phi_ref(j,nx-1,t_int))/(21600.0) * (t_sim - (t_int * 6.0 * 3600.0))
      END DO
    
    
    
      !Linker Rand
      DO j = -1, ny+1
         phi(j,0) = phi_ref(j,0,t_int) + (phi_ref(j,0,t_int+1)  &
         - phi_ref(j,0,t_int))/(21600.0) * (t_sim - (t_int * 6.0 * 3600.0))
        
         phi(j,1) = phi_ref(j,1,t_int) + (phi_ref(j,1,t_int+1)  &
         - phi_ref(j,1,t_int))/(21600.0) * (t_sim - (t_int * 6.0 * 3600.0))
        
         phi(j,-1) = phi_ref(j,-1,t_int) + (phi_ref(j,-1,t_int+1)  &
         - phi_ref(j,-1,t_int))/(21600.0) * (t_sim - (t_int * 6.0 * 3600.0))
      END DO
    

    
      !Randwerte Zeta
      DO i = 0,nx
         DO j = 0,ny
            zeta_r(j,i) = (1/f(j)) * ((phi(j,i+1) - 2.0 * phi(j,i)  &
            + phi(j,i-1))/delta**2 + (phi(j+1,i) - 2.0 * phi(j,i)  &
            + phi(j-1,i))/delta**2)
         END DO
      END DO
    
      zeta(ny,0:nx) = zeta_r(ny,:)
      zeta(0,0:nx) = zeta_r(0,:)
      zeta(0:ny,nx) = zeta_r(:,nx)
      zeta(0:ny,0) = zeta_r(:,0)
    
      WRITE (*,*) "Rand", zeta(ny,0)

   END SUBROUTINE get_randwerte
   !----------------------------------------------------------------------------
   SUBROUTINE data_output(action, output)
   ! Data output to NetCDF file
   

      CHARACTER(LEN=*), INTENT(IN) :: action !< flag to steer routine 
      CHARACTER(LEN=*), INTENT(IN) :: output !< output file name prefix

      INTEGER, SAVE :: do_count=0      !< counting output
      INTEGER, SAVE :: id_dim_time     !< ID of dimension time
      INTEGER, SAVE :: id_dim_x        !< ID of dimension x
      INTEGER, SAVE :: id_dim_y        !< ID of dimension time
      INTEGER, SAVE :: id_file         !< ID of NetCDF file
      INTEGER, SAVE :: id_var_phi      !< ID of geopotential
      INTEGER, SAVE :: id_var_phiref   !< ID of reference geopotential
      INTEGER, SAVE :: id_var_time     !< ID of time
      INTEGER, SAVE :: id_var_u        !< ID of wind speed along x
      INTEGER, SAVE :: id_var_v        !< ID of wind speed along y
      INTEGER, SAVE :: id_var_x        !< ID of x
      INTEGER, SAVE :: id_var_y        !< ID of y
      INTEGER, SAVE :: id_var_zeta     !< ID of vorticity
      INTEGER, SAVE :: nc_stat         !< status flag of NetCDF routines

      INTEGER :: tt             !< time index of ref. geopot.

      REAL, DIMENSION(:), ALLOCATABLE :: netcdf_data_1d  !< 1D output array

      REAL, DIMENSION(:,:), ALLOCATABLE :: netcdf_data_2d  !< 2D output array

      
      CALL SYSTEM_CLOCK(count,count_rate)
         diff_DAT = REAL(count)/REAL(count_rate) 
      

      SELECT CASE (TRIM(action))

         !- Initialize output file
         CASE ('open')

            !- Delete any pre-existing output file
            OPEN(20, FILE=TRIM(output)//'.nc')
            CLOSE(20, STATUS='DELETE')

            !- Open file
            nc_stat = NF90_CREATE(TRIM(output)//'.nc', NF90_NOCLOBBER, id_file)
            IF (nc_stat /= NF90_NOERR)  PRINT*, '+++ netcdf error'

            !- Write global attributes
            nc_stat = NF90_PUT_ATT(id_file, NF90_GLOBAL,  &
                                    'Conventions', 'COARDS')
            nc_stat = NF90_PUT_ATT(id_file, NF90_GLOBAL,  &
                                    'title', 'barotropic nwp-model')

            !- Define time coordinate
            nc_stat = NF90_DEF_DIM(id_file, 'time', NF90_UNLIMITED,  &
                                    id_dim_time)
            nc_stat = NF90_DEF_VAR(id_file, 'time', NF90_DOUBLE,  &
                                    id_dim_time, id_var_time)
            nc_stat = NF90_PUT_ATT(id_file, id_var_time, 'units',  &
                                    'seconds since 1900-1-1 00:00:00')

            !- Define spatial coordinates
            nc_stat = NF90_DEF_DIM(id_file, 'x', nx+1, id_dim_x)
            nc_stat = NF90_DEF_VAR(id_file, 'x', NF90_DOUBLE,  &
                                    id_dim_x, id_var_x)
            nc_stat = NF90_PUT_ATT(id_file, id_var_x, 'units', 'meters')

            nc_stat = NF90_DEF_DIM(id_file, 'y', ny+1, id_dim_y)
            nc_stat = NF90_DEF_VAR(id_file, 'y', NF90_DOUBLE,  &
                                    id_dim_y, id_var_y)
            nc_stat = NF90_PUT_ATT(id_file, id_var_y, 'units', 'meters')

            !- Define output arrays
            nc_stat = NF90_DEF_VAR(id_file, 'phi', NF90_DOUBLE,           &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_phi)
            nc_stat = NF90_PUT_ATT(id_file, id_var_phi,  &
                                    'long_name', 'geopotential at 500hPa')
            nc_stat = NF90_PUT_ATT(id_file, id_var_phi,  &
                                    'short_name', 'geopotential')
            nc_stat = NF90_PUT_ATT(id_file, id_var_phi, 'units', 'm2/s2')

            nc_stat = NF90_DEF_VAR(id_file, 'phi_ref', NF90_DOUBLE,       &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_phiref)
            nc_stat = NF90_PUT_ATT(id_file, id_var_phiref,  &
                                    'long_name',            &
                                    'reference geopotential at 500hPa')
            nc_stat = NF90_PUT_ATT(id_file, id_var_phiref,  &
                                    'short_name', 'ref. geopotential')
            nc_stat = NF90_PUT_ATT(id_file, id_var_phiref, 'units', 'm2/s2')

            nc_stat = NF90_DEF_VAR(id_file, 'zeta', NF90_DOUBLE,          &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_zeta)
            nc_stat = NF90_PUT_ATT(id_file, id_var_zeta,  &
                                    'long_name', 'vorticity at 500hPa')
            nc_stat = NF90_PUT_ATT(id_file, id_var_zeta,  &
                                    'short_name', 'vorticity')
            nc_stat = NF90_PUT_ATT(id_file, id_var_zeta, 'units', '1/s')

            nc_stat = NF90_DEF_VAR(id_file, 'u', NF90_DOUBLE,             &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_u)
            nc_stat = NF90_PUT_ATT(id_file, id_var_u,  &
                                    'long_name',       &
                                    'u component of wind')
            nc_stat = NF90_PUT_ATT(id_file, id_var_u, 'short_name', 'u')
            nc_stat = NF90_PUT_ATT(id_file, id_var_u, 'units', 'm/s')

            nc_stat = NF90_DEF_VAR(id_file, 'v', NF90_DOUBLE,             &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_v)
            nc_stat = NF90_PUT_ATT(id_file, id_var_v,  &
                                    'long_name',       &
                                    'v component of wind')
            nc_stat = NF90_PUT_ATT(id_file, id_var_v, 'short_name', 'v')
            nc_stat = NF90_PUT_ATT(id_file, id_var_v, 'units', 'm/s')


            !- Leave define mode
            nc_stat = NF90_ENDDEF(id_file)

            !- Write axis to file
            ALLOCATE(netcdf_data_1d(0:nx))

            !- x axis
            DO  i = 0, nx
               netcdf_data_1d(i) = i * delta
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_x, netcdf_data_1d,  &
                                    start = (/1/), count = (/nx+1/))
            !- y axis
            DO  j = 0, ny
               netcdf_data_1d(j) = j * delta
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_y, netcdf_data_1d,  &
                                    start = (/1/), count = (/ny+1/))

            DEALLOCATE(netcdf_data_1d)


         !- Close NetCDF file
         CASE ('close')

            nc_stat = NF90_CLOSE(id_file)


         !- Write data arrays to file
         CASE ('write')

            ALLOCATE(netcdf_data_2d(0:nx,0:ny))

            do_count = do_count + 1

            ! !- Write time
            nc_stat = NF90_PUT_VAR(id_file, id_var_time, (/t_sim/),  &
                                    start = (/do_count/),               &
                                    count = (/1/))

            !- Write geopotential
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = phi(j,i)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_phi, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),          &
                                    count = (/nx+1, ny+1, 1/))

            ! !- Write reference geopotential
            tt =  INT(t_sim/(6.0 * 3600.0))  ! time index of global ERA modell
            
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = phi_ref(j,i,tt)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_phiref, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),             &
                                    count = (/nx+1, ny+1, 1/))

            !- Write vorticity
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = zeta(j,i)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_zeta, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),           &
                                    count = (/nx+1, ny+1, 1/))

            ! !- Write u
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = u(j,i)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_u, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),        &
                                    count = (/nx+1, ny+1, 1/))

            ! !- Write v
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = v(j,i)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_v, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),        &
                                    count = (/nx+1, ny+1, 1/))

            DEALLOCATE(netcdf_data_2d)


         !- Print error message if unknown action selected
         CASE DEFAULT

            WRITE(*, '(A)') ' ** data_output: action "'//  &
                              TRIM(action)//'"unknown!'

      END SELECT
      
      
      CALL SYSTEM_CLOCK(count,count_rate)
	     diff_DAT = REAL(count)/REAL(count_rate) - diff_DAT
        
      DAT_t = DAT_t + diff_DAT


   END SUBROUTINE data_output

   
    

END PROGRAM nwp_program
